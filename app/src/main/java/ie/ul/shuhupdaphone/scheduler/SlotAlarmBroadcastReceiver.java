package ie.ul.shuhupdaphone.scheduler;

/* Student name: Eoghan Conlon
 * Student id: 21310262
 * Partner name:
 * Partner id:
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;

import ie.ul.shuhupdaphone.R;

public class SlotAlarmBroadcastReceiver extends BroadcastReceiver {

	/*
	 * DEBUG and TAG are used in conjunction with the Log class which allows you to print messages to the LogCat window. For examples, 
	 * see how they are used in below code.
	 */
	static boolean DEBUG = true;
	static String TAG = "PvdV SlotalarmBCR";



	private final static String RINGER_MODE = "RingerMode";
	private final static String END_TIME_DATE= "Date object end time";
	
	private static PendingIntent mPendingAlarmIntent = null;

	private final static String SLOT_ALARM_SILENCE_ACTION = "ie.ul.shuhupdaphone.scheduler.SlotAlarmBroadcastReceiver.Silence";

	private final static String SLOT_ALARM_REVERT_ACTION = "ie.shuhupdaphone.scheduler.SlotActionBroadcastReceiver.Revert";

	AudioManager audioManager;
	    
	@Override
	public void onReceive(final Context context, Intent intent) {
		final AlarmManager am = (AlarmManager) context
				.getSystemService(Context.ALARM_SERVICE);

		Intent newIntent = new Intent();
		newIntent.setAction(SLOT_ALARM_SILENCE_ACTION);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(context,
				0, newIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		int ringerMode;

		if (intent.getAction().equals("android.intent.action.BOOT_COMPLETED")) {
			// Cover the case where the phone has just rebooted ...
			try {
				am.cancel(pendingIntent);
			} catch (Exception e) {

			} 
		} 
		else if (intent.getAction().equals(SLOT_ALARM_SILENCE_ACTION)) {

			/*
			 * Exercise III:
			 * 	1: obtain current ringer mode
			 * 	2: set new ringer mode
			 */
			ringerMode = this.getRingerMode(context);
			audioManager = (AudioManager)  context.getSystemService(Context.AUDIO_SERVICE);
			setRingerMode(context, audioManager.RINGER_MODE_SILENT);
			Bundle extras = intent.getExtras();
			if (extras != null) {
				long endTime = extras.getLong(END_TIME_DATE) + 60000;
				scheduleRevert(context, endTime, ringerMode);
			}

		}
		else if (intent.getAction().equals(SLOT_ALARM_REVERT_ACTION)) {
			/*
			 * Exercise VI:
			 * 1: Revert to old ringer mode
			 */
			Bundle extras = intent.getExtras();
			if(extras != null){
				ringerMode = extras.getInt(RINGER_MODE);
				setRingerMode(context, ringerMode);
			}
		}

	}

	
		
	public static boolean scheduleNextAlarm(Context appContext)
    {
		Intent intent = new Intent();
	    intent.setAction(SLOT_ALARM_REVERT_ACTION);
	    mPendingAlarmIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	    AlarmManager mAlarmManager = (AlarmManager) appContext
			.getSystemService(Context.ALARM_SERVICE);
	    try
		{
			mAlarmManager.cancel(mPendingAlarmIntent);
		}
	    catch (Exception e)
		{
			//do nothing
		}
		
	    intent = new Intent();
	    intent.setAction(SLOT_ALARM_SILENCE_ACTION);
		/*
		 * Exercise V:
		 * 1: Change the time at which the phone reverts back to the original ringer mode
		 */
		long endTime = System.currentTimeMillis();
		intent.putExtra(END_TIME_DATE, endTime);
		mPendingAlarmIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
	        
		try
		{
			mAlarmManager.cancel(mPendingAlarmIntent);
		}
		catch (Exception e)
		{
			//do nothing
		}
		finally {
			long startTime = System.currentTimeMillis();
			mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, startTime, mPendingAlarmIntent);
			if (DEBUG) Log.e(TAG, "Silence phone at: " + convertCurrentTimeMillis(startTime));
		}
		return true;
    }
	
	/*
	 * Method should be called after every time the phone has been put into silence mode to revert to old setting at end of slot
	 */
	private static void scheduleRevert(Context appContext, long endTime, int mode) {
		Intent intent = new Intent();
        intent.setAction(SLOT_ALARM_REVERT_ACTION);
        intent.putExtra(RINGER_MODE, mode);
        mPendingAlarmIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager mAlarmManager = (AlarmManager) appContext
				.getSystemService(Context.ALARM_SERVICE);
        try 
        {
           mAlarmManager.cancel(mPendingAlarmIntent);
        } 
        catch (Exception e) 
        {
        	//do nothing
        } 
        finally 
        {
        	
            mAlarmManager.setExact(AlarmManager.RTC_WAKEUP, endTime, mPendingAlarmIntent);
            if (DEBUG) Log.e(TAG, "Revert phone at " + convertCurrentTimeMillis(endTime));
        }
	}
    
    public static void tearDownAlarmManager(Context appContext)
    {
    	AlarmManager mAlarmManager = (AlarmManager) appContext
				.getSystemService(Context.ALARM_SERVICE);
        try 
        {
           mAlarmManager.cancel(mPendingAlarmIntent);
        } 
        catch (Exception e) 
        {
        } 
     }

    public static String convertCurrentTimeMillis(long millis) {
		SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
		Date convertedDate = new Date(millis);
		return sdf.format(convertedDate);
	}

    /*
     * Set the ringerMode to 'mode' 
     */
    private void setRingerMode(Context context, int mode) {
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		audioManager.setRingerMode(mode);
    }
    
    /*
     * Get the current ringer mode
     */
    private int getRingerMode(Context context) {
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		return audioManager.getRingerMode();
    }

	
	

}
