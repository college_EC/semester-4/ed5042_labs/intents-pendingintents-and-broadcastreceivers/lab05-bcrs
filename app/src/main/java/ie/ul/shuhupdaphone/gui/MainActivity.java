/* Student name: 
 * Student id:
 * Partner name:
 * Partner id:
 */

package ie.ul.shuhupdaphone.gui;
import ie.ul.shuhupdaphone.R;
import ie.ul.shuhupdaphone.scheduler.SlotAlarmBroadcastReceiver;
import java.util.ArrayList;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity {

	Button startSilenceButton;


	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_relative);

        startSilenceButton = (Button) findViewById(R.id.startSilenceButton);

        startSilenceButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				SlotAlarmBroadcastReceiver.scheduleNextAlarm(getApplicationContext());
			}
        });

	}

 
    @Override
		protected void onRestoreInstanceState(Bundle savedInstanceState) {
		}


		@Override
		protected void onSaveInstanceState(Bundle outState) {
		}

    
}
